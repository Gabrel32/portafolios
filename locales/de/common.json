{
  "header": {
    "title": "Mein Portfolio",
    "nav": {
      "home": "Startseite",
      "projects": "Projekte",
      "about": "Fähigkeiten",
      "contact": "Kontakt"
    }
  },
  "home": {
    "title1": "Webentwicklung",
    "title2": "Entwicklung",
    "subtitle": "Gabriel Hernández",
    "description": "Spezialisiert auf die Entwicklung innovativer und skalierbarer Web-Lösungen, die modernes Design mit Spitzentechnologie kombinieren, um außergewöhnliche Benutzererfahrungen zu bieten.",
    "downloadCV": "Lebenslauf herunterladen",
    "aboutTitle": "Über mich",
    "contactButton": "Kontakt",
    "ctaTitle": "Bereit zur Zusammenarbeit?",
    "aboutDescription": "Ich bin ein Frontend-Entwickler, der leidenschaftlich daran arbeitet, komplexe Ideen in ansprechende und funktionale digitale Erlebnisse zu verwandeln. Mit über 2 Jahren beruflicher Erfahrung habe ich mit Fachleuten an innovativen Projekten wie einem interaktiven Mathematikbuch gearbeitet und an dynamischen Webanwendungen mit React, Node.js und modernen Technologien mitgewirkt. Mein Ansatz vereint Kreativität, technische Präzision und einen Hauch von Mathematik, um Schnittstellen zu schaffen, die nicht nur gut aussehen, sondern auch echte Probleme lösen."
  },
  "projectsSection": {
    "title": "Meine Projekte",
    "description": "Hier können Sie einige meiner herausragenden Projekte sehen",
    "viewDetails": "Details anzeigen",
    "technologiesUsed": "Verwendete Technologien",
    "visitProject": "Projekt besuchen",
    "viewCode": "Code ansehen"
  },
  "about": {
    "title": "Über mich",
    "description": "Technologien, mit denen ich arbeite",
    "alsoWorkedWith": "Ich habe auch mit",
    "orms": "ORMs",
    "like": "wie",
    "andOthers": ", unter anderem, gearbeitet."
  },
  "contact": {
    "title": "Kontaktieren Sie mich",
    "formTitle": "Schicken Sie mir eine Nachricht",
    "linkedinText": "Oder senden Sie mir eine direkte Nachricht über"
  },
  "contactForm": {
    "successTitle": "Erfolg!",
    "successDescription": "Ihre Nachricht wurde erfolgreich gesendet. Ich werde mich bald mit Ihnen in Verbindung setzen, um Ihre Ideen oder Projekte zu besprechen!",
    "errorTitle": "Fehler!",
    "errorDescription": "Wir konnten Ihre Nachricht derzeit nicht senden. Bitte überprüfen Sie Ihre Verbindung oder versuchen Sie es in ein paar Minuten erneut.",
    "labels": {
      "name": "Name",
      "email": "E-Mail",
      "message": "Nachricht"
    },
    "placeholders": {
      "name": "Ihr Name",
      "email": "Ihre E-Mail"
    },
    "errors": {
      "nameRequired": "Bitte geben Sie Ihren Namen ein",
      "emailRequired": "Bitte geben Sie Ihre E-Mail-Adresse ein",
      "invalidEmail": "Bitte geben Sie eine gültige E-Mail-Adresse ein",
      "messageRequired": "Bitte wählen Sie eine Nachricht aus",
      "sendError": "Fehler beim Senden der Nachricht. Versuchen Sie es erneut.",
      "connectionError": "Verbindungsfehler. Überprüfen Sie Ihre Internetverbindung."
    },
    "selectMessage": "Nachricht auswählen",
    "sending": "Wird gesendet...",
    "sendMessage": "Nachricht senden",
    "successMessage": "Nachricht erfolgreich gesendet!",
    "predefinedMessages": [
      "Hallo, ich möchte mehr Informationen über Ihre Dienstleistungen.",
      "Ich bin daran interessiert, mit Ihnen zusammenzuarbeiten. Können wir sprechen?",
      "Ich habe eine Frage zu einem Projekt, das ich besprechen möchte.",
      "Ich würde gerne mit Ihnen an einem zukünftigen Projekt zusammenarbeiten.",
      "Könnten Sie mir bei einer technischen Frage helfen?",
      "Ich möchte ein Angebot für einen maßgeschneiderten Service erhalten.",
      "Sind Sie für neue Möglichkeiten verfügbar?",
      "Ich benötige professionelle Beratung zu einem bestimmten Thema.",
      "Bieten Sie Beratungsdienste an?",
      "Ich möchte ein virtuelles Treffen vereinbaren.",
      "Haben Sie Referenzen von früheren Arbeiten?",
      "Ich möchte mehr über Ihre Berufserfahrung erfahren.",
      "Könnten Sie Beispiele für ähnliche Projekte teilen?",
      "Ich prüfe Optionen für mein Unternehmen, können wir sprechen?",
      "Wie hoch ist Ihr Stundensatz?"
    ]
  },
  "projectsPage": {
    "title": {
      "first": "Meine",
      "second": "Projekte"
    },
    "loading": "Projekte werden geladen...",
    "empty": "Keine Projekte verfügbar."
  },
  "projects": {
    "guitars-la": {
      "name": "GuitarLA",
      "description": "Eine voll funktionsfähige E-Commerce-Plattform, entwickelt mit Next.js, die fortschrittliche Paginierung, nahtlose PostgreSQL-Datenbankintegration und ein dynamisches Warenkorbsystem für ein verbessertes Benutzererlebnis bietet."
    },
    "product-hunt": {
      "name": "Product Hunt Klon",
      "description": "Eine Echtzeit-Replik von Product Hunt, die Firebase für Authentifizierung und Datenspeicherung nutzt, Next.js für serverseitiges Rendering und Styled Components für eine elegante, moderne Benutzeroberfläche."
    },
    "mern-uptask": {
      "name": "MERN-Uptask",
      "description": "Eine Echtzeit-Aufgabenverwaltungsanwendung, die den MERN-Stack (MongoDB, Express, React, Node.js) und Socket.io für sofortige Aufgabenaktualisierungen und kollaborative Funktionen verwendet."
    },
    "veterinary-control": {
      "name": "Veterinärkontrolle",
      "description": "Ein umfassendes Terminverwaltungssystem für Tierkliniken, entwickelt mit React und TailwindCSS, um Klinikabläufe zu optimieren und die Planungseffizienz zu verbessern."
    },
    "create-interactive": {
      "name": "Interaktiv Erstellen",
      "description": "Eine Offline-First CRUD-Anwendung, die IndexDB für robuste lokale Datenspeicherung nutzt und ein reibungsloses, unterbrechungsfreies Benutzererlebnis auch ohne Internetverbindung gewährleistet."
    },
    "expense-manager": {
      "name": "Ausgabenmanager",
      "description": "Ein persönliches Finanzverfolgungstool, entwickelt mit React und localStorage, das Benutzern ermöglicht, Budgets effektiv mit persistentem Datenspeicher und intuitiven Steuerungen zu verwalten."
    },
    "drink-finder": {
      "name": "Getränkefinder",
      "description": "Eine Cocktail-Entdeckungs-App, die mit einer externen API integriert ist, React Context für effizientes Zustandsmanagement verwendet und ein responsives Design mit Bootstrap bietet."
    },
    "weather-app": {
      "name": "Wetter-App",
      "description": "Ein Echtzeit-Wetter-Dashboard, entwickelt mit React Context und TailwindCSS, das präzise und aktuelle globale Wettervorhersagen in einer klaren, benutzerfreundlichen Oberfläche bietet."
    },
    "car-insurance": {
      "name": "Autoversicherungsrechner",
      "description": "Ein dynamischer Autoversicherungsprämienrechner, entwickelt mit React Context und TailwindCSS, der eine Echtzeit-Analyse verschiedener Faktoren bietet, um genaue Versicherungsschätzungen zu liefern."
    },
    "news-finder": {
      "name": "Nachrichtenfinder",
      "description": "Ein Nachrichtenaggregator mit mehreren Kategorien, der React Context für das Zustandsmanagement und Material-UI für eine moderne, responsive und visuell ansprechende Benutzeroberfläche nutzt."
    },
    "instant-cryptos": {
      "name": "Instant Cryptos",
      "description": "Eine Echtzeit-Kryptowährungsverfolgungsplattform mit integriertem Währungsrechner und fortschrittlichen Marktanalysetools, entwickelt für Gelegenheitsnutzer und ernsthafte Investoren."
    }
  },
  "project": {
    "buttonST": "Seite"
  },
  "layout": {
    "siteTitle": "Portfolio",
    "siteDescription": "Webportfolio, in dem Sie verschiedene Projekte und relevante Informationen finden.",
    "defaultPageTitle": "Seite",
    "footerRights": "Alle Rechte vorbehalten."
  },
  "languageSwitcher": {
    "spanish": "Spanisch",
    "english": "Englisch"
  },
  "themeSwitcher": {
    "ariaLabel": "Themenwähler",
    "themes": {
      "default": "Klassisch",
      "forest": "Wald",
      "ocean": "Ozean",
      "desert": "Wüste",
      "sunset": "Sonnenuntergang"
    }
  }
}